package com.geekhub.services;

import com.geekhub.entities.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MoviesService {
    private static final String CREATE_QUERY = "INSERT INTO movies (name, year) VALUES (?, ?)";
    private static final String UPDATE_QUERY = "UPDATE movies SET name=?, year=? WHERE id=?";
    private static final String DELETE_QUERY = "DELETE FROM movies WHERE id=?";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM movies WHERE id=?";
    private static final String SELECT_ALL = "SELECT movies.id, movies.name, movies.year FROM `movies`";
    private static final String GET_COUNT_ACTORS = "SELECT movies.id, COUNT( actormovie.id_actor ) FROM `movies` " +
            "LEFT JOIN actormovie ON actormovie.id_movie = movies.id GROUP BY movies.id";

    @Autowired
    protected javax.sql.DataSource dataSource;

    @Autowired
    public MoviesService(DataSource dataSource) throws SQLException {
        this.dataSource = dataSource;
    }

    public Movie create(Movie movie) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, movie.getName());
            statement.setDate(2, new java.sql.Date(movie.getYear().getTime()));
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            return movie.withId(rs.getInt(1));
        }
    }

    public Movie getById(int id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return new Movie().withId(rs.getInt(1))
                    .withName(rs.getString(2))
                    .withYear(rs.getDate(3));
        }
    }

    public void update(Movie movie) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY)) {
            statement.setString(1, movie.getName());
            statement.setDate(2, new java.sql.Date(movie.getYear().getTime()));
            statement.setInt(3, movie.getId());
            statement.executeUpdate();
        }
    }

    public boolean delete(int id) throws SQLException {
        try (PreparedStatement statement = dataSource.getConnection().prepareStatement(DELETE_QUERY)) {
            statement.setInt(1, id);
            return statement.execute();
        }
    }

    public List<Movie> getMovies() throws SQLException {
        List<Movie> res = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SELECT_ALL);
            while (rs.next()) {
                res.add(new Movie().withId(rs.getInt(1))
                        .withName(rs.getString(2))
                        .withYear(rs.getDate(3)));
            }
        }
        return res;
    }

    public Map<Integer, Integer> getCountActors() throws SQLException {
        Map<Integer, Integer> res = new HashMap<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(GET_COUNT_ACTORS);
            while (rs.next()) {
                res.put(rs.getInt(1), rs.getInt(2));
            }
        }
        return res;
    }
}
