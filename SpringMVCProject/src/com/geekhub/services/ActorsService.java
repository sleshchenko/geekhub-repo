package com.geekhub.services;

import com.geekhub.entities.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ActorsService {
    private static final String CREATE_QUERY = "INSERT INTO actors (name, surname, birth) VALUES (?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE actors SET name=?, surname=?, birth=? WHERE id=?";
    private static final String DELETE_QUERY = "DELETE FROM actors WHERE id=?";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM actors WHERE id=?";
    private static final String SELECT_ALL = "SELECT actors.id, actors.name, actors.surname, actors.birth FROM `actors`";
    private static final String GET_COUNT_FILMS = "SELECT actors.id, COUNT( actormovie.id_movie ) FROM `actors` " +
            "LEFT JOIN actormovie ON actormovie.id_actor = actors.id GROUP BY actors.id";
    private static final String ADD_LINK_QUERY = "INSERT INTO actormovie (id_actor, id_movie) VALUES (?, ?)";
    private static final String SELECT_LINK_QUERY = "SELECT * FROM actormovie WHERE id_actor = ?";

    @Autowired
    protected javax.sql.DataSource dataSource;

    @Autowired
    public ActorsService(DataSource dataSource) throws SQLException {
        this.dataSource = dataSource;
    }

    public Actor create(Actor actor) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, actor.getName());
            statement.setString(2, actor.getSurname());
            statement.setDate(3, new Date(actor.getBirth().getTime()));
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            return actor.withId(rs.getInt(1));
        }
    }

    public void updateLinks(int idActor, Integer[] movies) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            connection.createStatement().executeUpdate("DELETE FROM actormovie WHERE id_actor = " + idActor);
        }
        for (int idMovie : movies) {
            addLink(idActor, idMovie);
        }
    }

    public void addLink(int idActor, int idMovie) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM actormovie WHERE id_actor = ? AND id_movie = ?");
            statement.setInt(1, idActor);
            statement.setInt(2, idMovie);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return;
            }
            statement = connection.prepareStatement(ADD_LINK_QUERY);
            statement.setInt(1, idActor);
            statement.setInt(2, idMovie);
            statement.executeUpdate();
        }
    }

    public Map<Integer, Object> getMovies(int idActor) throws SQLException {
        Map<Integer, Object> res = new HashMap<>();
        Object fake = new Object();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_LINK_QUERY)) {
            statement.setInt(1, idActor);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                res.put(rs.getInt(2), fake);
            }
        }
        return res;
    }

    public Actor getById(int id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return new Actor().withId(rs.getInt(1))
                    .withName(rs.getString(2))
                    .withSurname(rs.getString(3))
                    .withBirth(rs.getDate(4));
        }
    }

    public void update(Actor actor) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY)) {
            statement.setString(1, actor.getName());
            statement.setString(2, actor.getSurname());
            statement.setDate(3, new Date(actor.getBirth().getTime()));
            statement.setInt(4, actor.getId());
            statement.executeUpdate();
        }
    }

    public boolean delete(int id) throws SQLException {
        try (PreparedStatement statement = dataSource.getConnection().prepareStatement(DELETE_QUERY)) {
            statement.setInt(1, id);
            return statement.execute();
        }
    }

    public List<Actor> getActors() throws SQLException {
        List<Actor> res = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SELECT_ALL);
            while (rs.next()) {
                res.add(new Actor().withId(rs.getInt(1))
                        .withName(rs.getString(2))
                        .withSurname(rs.getString(3))
                        .withBirth(rs.getDate(4)));
            }
        }
        return res;
    }

    public Map<Integer, Integer> getCountFilms() throws SQLException {
        Map<Integer, Integer> res = new HashMap<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(GET_COUNT_FILMS);
            while (rs.next()) {
                res.put(rs.getInt(1), rs.getInt(2));
            }
        }
        return res;
    }
}
