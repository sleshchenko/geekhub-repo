package com.geekhub.entities;

import java.util.Date;

public class Movie {
    private int id;
    private String name;
    private Date year;

    public Movie(String name, Date year) {
        this.name = name;
        this.year = year;
    }

    public Movie() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Movie withName(String name) {
        this.name = name;
        return this;
    }

    public Movie withYear(Date year) {
        this.year = year;
        return this;
    }

    public Movie withId(int id) {
        this.id = id;
        return this;
    }


    public String getName() {
        return name;
    }

    public Date getYear() {
        return year;
    }

    public int getId() {
        return id;
    }
}
