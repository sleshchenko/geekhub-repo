<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>
<div class="wrapper">
    <div id="menu">
        <ul>
            <li><a href="/actors" class="link">Actors</a></li>
            <li><a href="/movies" class="linkCurrent">Movies</a></li>
        </ul>
    </div>
    <div class="body">
        <center>
            <table class="table">
                <tr class="tableHeader">
                    <td>Name</td>
                    <td>Year</td>
                    <td># of Actors</td>
                </tr>
                <c:forEach var="movie" items="${movies}">
                    <tr>
                        <td><c:out value="${movie.name}"/></td>
                        <td><fmt:formatDate type="both" pattern="yyyy" value="${movie.year}" /></td>
                        <td><c:out value="${countActors[movie.id]}"/></td>
                        <td style="width: 40px;"><a href="/movie?id=${movie.id}" class="linkEdit">Edit</a></td>
                        <td><a href="/deletemovie?id=${movie.id}" class="linkDelele">X</a></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="6" style="text-align: right;">
                        <input type="button" style="margin-right: 100px; margin-top: 20px;" value="Add New Movie" onclick="location='/movie'"/>
                    </td>
                </tr>
            </table>
        </center>
    </div>
</div>
</body>
</html>