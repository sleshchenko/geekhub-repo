<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>
<div class="wrapper">
    <div id="menu">
        <ul>
            <li><a href="/actors" class="linkCurrent">Actors</a></li>
            <li><a href="/movies" class="link">Movies</a></li>
        </ul>
    </div>
    <div class="body">
        <center>
            <table class="table">
                <tr class="tableHeader">
                    <td>First name</td>
                    <td>Last name</td>
                    <td>Birth date</td>
                    <td># of Movies</td>
                </tr>
                <c:forEach var="actor" items="${actors}">
                    <tr>
                        <td><c:out value="${actor.name}"/></td>
                        <td><c:out value="${actor.surname}"/></td>
                        <td><fmt:formatDate type="both" pattern="dd/MM/yyyy" value="${actor.birth}" /></td>
                        <td><c:out value="${countFilms[actor.id]}"/></td>
                        <td style="width: 40px;"><a href="/actor?id=${actor.id}" class="linkEdit">Edit</a></td>
                        <td><a href="/deleteactor?id=${actor.id}" class="linkDelele">X</a></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="6" style="text-align: right;">
                        <input type="button" style="margin-right: 100px; margin-top: 20px;" value="Add New Actor" onclick="location='/actor'"/>
                    </td>
                </tr>
            </table>
        </center>
    </div>
</div>
</body>
</html>