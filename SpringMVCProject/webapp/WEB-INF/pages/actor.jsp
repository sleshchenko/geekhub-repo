<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>
<div class="wrapper">
    <form method="POST" action="/updateactor">
        <div class="body">
            <center>
                <table class="table" style="font-size: 20px;">
                    <tr class="tableHeader">
                        <td colspan="2" style="text-align: center">Add/Edit Actor</td>
                    </tr>
                    <input name="id" value="<c:if test="${not empty actor.id}">${actor.id}</c:if>" style="display: none;"/>
                    <tr>
                        <td>First name</td>
                        <td><input class="input" name="name"
                                   value="<c:if test="${not empty actor.id}">${actor.name}</c:if>"/></td>
                    </tr>
                    <tr>
                        <td>Last name</td>
                        <td><input class="input" name="surname"
                                   value="<c:if test="${not empty actor.id}">${actor.surname}</c:if>"/></td>
                    </tr>
                    <tr>
                        <td>Birth date</td>
                        <td><input class="input" name="birth" value='<c:if test="${not empty actor.id}"><fmt:formatDate type="both" pattern="dd/MM/yyyy" value="${actor.birth}" /></c:if>'/>
                        </td>
                    </tr>
                    <tr>
                        <td>Movies</td>
                        <td><select multiple size="15" style="width: 200px;" name="movies">
                            <c:forEach var="movie" items="${movies}">
                                <c:out value="${not empty selectedMovies[movie.id]}"></c:out>
                                <option value="${movie.id}" <c:if test="${not empty selectedMovies[movie.id]}">selected="true"</c:if>>${movie.name}</option>
                            </c:forEach>
                        </select></td>
                    </tr>
                    <tr style="text-align: center; height: 60px;">
                        <td>
                            <button type="submit" class="btn">Save</button>
                        </td>
                        <td>
                            <button class="btn" onclick="document.location = '/actors';">Cancel</button>
                        </td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</div>
</body>
</html>