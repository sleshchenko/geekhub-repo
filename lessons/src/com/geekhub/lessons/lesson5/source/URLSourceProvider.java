package com.geekhub.lessons.lesson5.source;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) throws MalformedURLException {
        try(InputStream iStream = new URL(pathToSource).openStream()){
            iStream.read();
        } catch(IOException e){
            return false;
        } 
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String res = "";

        try(BufferedReader is = new BufferedReader(new InputStreamReader(new URL(pathToSource).openStream()))){
            String tmp;
            while( (tmp = is.readLine()) != null ){
                res += tmp + "\n";
            }
        }

        return res;
    }
}
