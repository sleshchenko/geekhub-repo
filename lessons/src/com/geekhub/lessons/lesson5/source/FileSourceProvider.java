package com.geekhub.lessons.lesson5.source;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        return new File(pathToSource).canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String res = "";

        try(BufferedReader is = new BufferedReader(new FileReader(new File(pathToSource)))){
            String tmp;
            while( (tmp = is.readLine()) != null ){
                res += tmp + "\n";
            }
        }

        return res;
    }
}