package com.geekhub.lessons.lesson5;

import com.geekhub.lessons.lesson5.source.SourceLoader;
import com.geekhub.lessons.lesson5.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input url to web resource or absolute paths to local file system or `exit`.");
        String command = scanner.next();

        while(!"exit".equals(command)) {
            //      So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);

                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);
            } catch (IOException e) {
                System.out.println("Can`t translate this text. Try enter another path.");
            }

            System.out.println("Please input url to web resource or absolute paths to local file system or `exit`.");
            command = scanner.next();
        }
    }
}
