package com.geekhub.lessons.lesson4.task2;

import java.util.Calendar;

public class Test {
    public static void main(String[] args) {
        SortedTaskManager taskManager = new SortedTaskManager();
        Calendar calendarTmp = Calendar.getInstance();
        calendarTmp.set(2013, Calendar.NOVEMBER, 20);
        taskManager.addTask(calendarTmp.getTime(), new Task("Навчання", "Закодити ММДО"));

        calendarTmp.set(2013, Calendar.NOVEMBER, 20);
        taskManager.addTask(calendarTmp.getTime(), new Task("Спорт", "Турнічки <3"));

        calendarTmp.set(2014, Calendar.FEBRUARY, 15);
        taskManager.addTask(calendarTmp.getTime(), new Task("Спорт", "Тренажорка"));

        calendarTmp.set(2013, Calendar.DECEMBER, 10);
        taskManager.addTask(calendarTmp.getTime(), new Task("Курси", "GeekHub Hometask"));

        System.out.println(taskManager.getAllTaskAsString());

        System.out.println(taskManager.getTaskByCategoriesAsString());

        System.out.println(taskManager.getTaskByCategory("Курси"));

        calendarTmp.set(2013, Calendar.NOVEMBER, 20);
        taskManager.removeAllTaskForDay(calendarTmp.getTime());
        System.out.println();

        System.out.println(taskManager.getAllTaskAsString());
    }
}
