package com.geekhub.lessons.lesson4.task2;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeAllTaskForDay(Date date);
    public Collection<String> getCategories();

    public Map<String, List<Task> > getTasksByCategories();
    public List<Task> getTaskByCategory(String category);
    public List<Task> getTasksForToday();
}
