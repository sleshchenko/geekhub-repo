package com.geekhub.lessons.lesson4.task2;

import java.util.*;

public class SortedTaskManager implements TaskManager{
    private Map<Date, List<Task> > tasks = new TreeMap<Date, List<Task>>();

    private Date getFromDateYearHourlyDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    @Override
    public void addTask(Date date, Task task) {
        date = getFromDateYearHourlyDay(date);
        if (tasks.containsKey(date)) {
            tasks.get(date).add(task);
        }
        else {
            ArrayList<Task> arr = new ArrayList<Task>();
            arr.add(task);
            tasks.put(date, arr);
        }
    }

    @Override
    public void removeAllTaskForDay(Date date) {
        date = getFromDateYearHourlyDay(date);
        tasks.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> categories = new HashSet<String>();
        for (Map.Entry<Date, List<Task>> entry : tasks.entrySet()) {
            for (Task task : entry.getValue()) {
                categories.add(task.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> result = new HashMap<String, List<Task>>();
        for (Map.Entry<Date, List<Task> > entry : tasks.entrySet()) {
            for (Task task : entry.getValue()) {
                if (result.containsKey(task.getCategory())) {
                    result.get(task.getCategory()).add(task);
                }
                else {
                    List<Task> list = new ArrayList<Task>();
                    list.add(task);
                    result.put(task.getCategory(), list);
                }
            }
        }
        return result;
    }

    @Override
    public List<Task> getTaskByCategory(String category) {
        List<Task> result = new ArrayList<Task>();
        for (Map.Entry<Date, List<Task> > entry : tasks.entrySet()) {
            for (Task task : entry.getValue()) {
                if (task.getCategory().equals(category)) {
                    result.add(task);
                }
            }
        }
        return result;
    }

    @Override
    public List<Task> getTasksForToday() {
        return tasks.get(getFromDateYearHourlyDay(new Date()));
    }

    // Next function are used for get information as String for SystemOutPrintln

    public String getAllTaskAsString() {
        String result = "";
        for (Map.Entry<Date, List<Task> > entry : tasks.entrySet()) {
            result += "--->>>> Tasks for " + entry.getKey() + "\n";
            for (Task task : entry.getValue()) {
                result += task + "\n";
            }
        }
        return result;
    }

    public String getTaskByCategoriesAsString() {
        Map<String, List<Task>> input = getTasksByCategories();
        String result = "";
        for (Map.Entry<String, List<Task>> entry : input.entrySet()) {
            result += "--->>> Tasks with categories " + entry.getKey() + "\n";
            for (Task task : entry.getValue()) {
                result += task.getDescription() + "\n";
            }
        }
        return result;
    }
}
