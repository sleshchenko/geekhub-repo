package com.geekhub.lessons.lesson4.task2;

import java.util.Date;

public class Task {
    private String category;
    private String description;

    public Task(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return "Category: " + category + ". Description: " + description;
    }

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);
    }
}
