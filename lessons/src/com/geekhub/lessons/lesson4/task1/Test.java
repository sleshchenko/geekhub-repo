package com.geekhub.lessons.lesson4.task1;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Test {
    public static void test(SetOperations forOperation, Set a, Set b) {
        System.out.println("a =" + a);
        System.out.println("b =" + b);
        long now = System.nanoTime();
        System.out.println("a equal b ? " + forOperation.equals(a,b) + ". time: " + (System.nanoTime() - now));
        now = System.nanoTime();
        System.out.println("union(a,b) = " + forOperation.union(a,b)  + ". time: " + (System.nanoTime() - now));
        now = System.nanoTime();
        System.out.println("subtract(a,b) = " + forOperation.subtract(a, b) + ". time: " + (System.nanoTime() - now));
        now = System.nanoTime();
        System.out.println("intersect(a,b) = " + forOperation.intersect(a, b)  + ". time: " + (System.nanoTime() - now));
        now = System.nanoTime();
        System.out.println("symmetricSubtract(a,b) = " + forOperation.symmetricSubtract(a, b) + ". time: " +
                (System.nanoTime() - now));
    }


    public static void main(String[] args) {
        Set<Integer> a = new HashSet<Integer>();
        Set<Integer> b = new HashSet<Integer>();

        SetOperations forOperation = new ExtendedSetForEach();

        Collections.addAll(a, 1,2,3,4);
        Collections.addAll(b, 1,2,3,4);
        System.out.println("------------>>>> TEST 1 <<<<<----------------");
        test(forOperation, a, b);

        b.clear();
        Collections.addAll(b, 3,4,5,6);

        System.out.println("------------>>>> TEST 2 <<<<<----------------");
        test(forOperation, a, b);

        b.clear();
        Collections.addAll(b, 5,6,7,8);

        System.out.println("------------>>>> TEST 3 <<<<<----------------");
        test(forOperation, a, b);

    }
}
