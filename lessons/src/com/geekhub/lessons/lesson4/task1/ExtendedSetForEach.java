package com.geekhub.lessons.lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class ExtendedSetForEach implements SetOperations{

    @Override
    public boolean equals(Set a, Set b) {
        if (a.size() != b.size())
            return false;

        for (Object oneObj : a) {
            if (! b.contains(oneObj) ) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set union(Set a, Set b) {
        Set<Object> res = new HashSet<Object>();

        for (Object oneObj : a) {
            res.add(oneObj);
        }

        for (Object oneObj : b) {
            res.add(oneObj);
        }

        return res;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set<Object> res = new HashSet<Object>();

        for (Object oneObj : a) {
            if ( !b.contains(oneObj) )
                res.add(oneObj);
        }

        return res;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set<Object> res = new HashSet<Object>();

        for (Object oneObj : a) {
            if ( b.contains(oneObj) )
                res.add(oneObj);
        }

        return res;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set<Object> res = new HashSet<Object>();

        for (Object oneObj : a) {
            if ( !b.contains(oneObj) )
                res.add(oneObj);
        }

        for (Object oneObj : b) {
            if ( !a.contains(oneObj) )
                res.add(oneObj);
        }

        return res;
    }
}
