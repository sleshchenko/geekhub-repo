package com.geekhub.lessons.lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class ExtendedSet implements SetOperations{

    @Override
    public boolean equals(Set a, Set b) {
        if (a.size() != b.size())
            return false;

        if (!a.containsAll(b))
            return false;

        return true;
    }

    @Override
    public Set union(Set a, Set b) {
        Set<Object> res = new HashSet<Object>(a);
        res.addAll(b);
        return res;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set<Object> res = new HashSet<Object>(a);
        res.removeAll(b);
        return res;
    }

    @Override
    public Set intersect(Set a, Set b) {
        return subtract(union(a,b), symmetricSubtract(a,b));
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a, b), subtract(b, a));
    }
}