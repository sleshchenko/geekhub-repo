package utils;

public class ExplorerUtils {
    public static String getPathToUpOneLevel(String path) {
        return path.lastIndexOf("\\") == -1 ? "" : path.substring(0, path.lastIndexOf("\\"));
    }
}
