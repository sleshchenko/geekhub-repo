package com.geekhub.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.ExplorerUtils.getPathToUpOneLevel;

public class ServletViewer extends HttpServlet {

    private String ROOT;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init();
        this.ROOT = servletConfig.getServletContext().getInitParameter("root");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String pathParam = request.getParameter("path");

        if (pathParam == null) {
            pathParam = "";
        }

        String absolutePath = getPathToUpOneLevel(System.getProperty("user.dir")) + "\\"+ROOT+"\\" + pathParam;

        response.setContentType("text/html");

        File file = new File(absolutePath);

        writer.println("<a href='/explore?path="+getPathToUpOneLevel(pathParam)+"'>To explorer</a><br>");
        writer.println("<h3>Content of file "+ ROOT +pathParam+"</h3>");
        try (FileReader reader = new FileReader(file)) {
            char[] buffer = new char[1024];
            while (reader.read(buffer) != -1) {
                writer.write(new String(buffer).replaceAll("(\r\n)|\r|\n", "<br>"));
            }
        }
    }


}
