package com.geekhub.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.ExplorerUtils.getPathToUpOneLevel;

public class ServletExplorer extends HttpServlet {

    private String ROOT;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init();
        this.ROOT = servletConfig.getServletContext().getInitParameter("root");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String pathParam = request.getParameter("path");

        if (pathParam == null) {
            pathParam = "";
        }

        String absolutePath = getPathToUpOneLevel(System.getProperty("user.dir")) + "\\"+ROOT+"\\" + pathParam;

        response.setContentType("text/html");

        StringBuilder resFolders = new StringBuilder("");
        StringBuilder resFiles = new StringBuilder("");
        File folder = new File(absolutePath);

        writer.println("Current location: "+ ROOT +pathParam+"<br>");

        writer.println("<a href='/explore?path="+ getPathToUpOneLevel(pathParam)+"'>Folder up</a><br>");

        String viewDir = "<a href='/explore?path=%s'>%s</a><br>";
        String viewFile = "<a href='/view?path=%s'>%s</a><br>";

        for (File oneItem : folder.listFiles()) {
            if (oneItem.isDirectory()) {
                resFolders.append(String.format(viewDir, pathParam+"\\"+oneItem.getName(),oneItem.getName()));
            } else {
                resFiles.append(String.format(viewFile, pathParam+"\\"+oneItem.getName(),oneItem.getName()));
            }
        }

        writer.println("<h1>Folders:</h1>");
        writer.println(resFolders.toString());
        writer.println("<h1>Files:</h1>");
        writer.println(resFiles.toString());
    }
}
