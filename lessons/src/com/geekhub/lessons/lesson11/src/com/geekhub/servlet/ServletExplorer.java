package com.geekhub.servlet;

import com.geekhub.Translator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletExplorer extends HttpServlet {
    private Translator translator;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        translator = context.getBean(Translator.class);
    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String text = request.getParameter("text");
        if (text != null && !text.isEmpty()) {
            String translatedText = translator.translate(text);
            request.setAttribute("translatedText", translatedText);
        }
        request.getRequestDispatcher("WEB-INF/translator.jsp").forward(request, response);
    }
}
