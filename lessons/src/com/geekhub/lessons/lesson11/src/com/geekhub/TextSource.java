package com.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TextSource {

    @Autowired
    private ResourceLoader resourceLoader;

	public String getText(String path) {
		List<String> load = resourceLoader.load(path);
		StringBuilder sb = new StringBuilder();
		for (String s : load) {
			sb.append(s);
		}
		return sb.toString();
	}
}
