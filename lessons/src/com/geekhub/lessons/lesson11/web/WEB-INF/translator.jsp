<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Translator</title>
</head>
<body>

<form method="POST" action="/translator">
    <div style="width: auto; margin-bottom: 30px;">
        <textarea name="text" style="width: 45%; margin-right: 3%; margin-left: 3%; height: 400px;" ></textarea>
        <textarea name="text" style="width: 45%; height: 400px;"><c:out value="${translatedText}"></c:out></textarea>
    </div>
    <center><button type="submit" style="font-size: 20px;">Перекласти</button></center>
</form>

</body>
</html>