package com.geekhub.lessons.lesson3.task3;

public class Main {

    public static long testStringBuilder(int lengthStrForConcat, int countIteration) {
        String forConcat;
        forConcat = "";
        for (int i = 0; i < lengthStrForConcat; i++) {
            forConcat += " ";
        }

        StringBuilder str = new StringBuilder();

        long end;
        long begin = System.currentTimeMillis();
        for (int i = 0; i < countIteration; i++) {
            str.append(forConcat);
        }
        end = System.currentTimeMillis();

        return end - begin;
    }

    public static long testStringBuffer(int lengthStrForConcat, int countIteration) {
        String forConcat = "";
        for (int i = 0; i < lengthStrForConcat; i++) {
            forConcat += " ";
        }

        StringBuffer str = new StringBuffer();

        long end;
        long begin = System.currentTimeMillis();
        for (int i = 0; i < countIteration; i++) {
            str.append(forConcat);
        }
        end = System.currentTimeMillis();

        return end - begin;
    }

    public static long testString(int lengthStrForConcat, int countIteration) {
        String forConcat = "";
        for (int i = 0; i < lengthStrForConcat; i++) {
            forConcat += " ";
        }

        String str = "";

        long end;
        long begin = System.currentTimeMillis();
        for (int i = 0; i < countIteration; i++) {
            str = str.concat(forConcat);
        }
        end = System.currentTimeMillis();

        return end - begin;
    }

    public static void testAll(int lengthStrForConcat, int countIteration) {
        System.out.println("Length string which is + = " + lengthStrForConcat + ", countIteration = " + countIteration);
        System.out.println("String = " + testString(lengthStrForConcat, countIteration));
        System.out.println("StringBuffer = " + testStringBuffer(lengthStrForConcat, countIteration));
        System.out.println("StringBuilder = " + testStringBuilder(lengthStrForConcat, countIteration));
    }

    public static void main(String[] args) {
        testAll(1000, 100);
        testAll(100, 1000);
        testAll(10, 10000);
        testAll(100, 10000);
        testAll(100, 10000);
        testAll(100, 10000);
    }
}
