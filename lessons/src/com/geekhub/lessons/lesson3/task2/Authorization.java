package com.geekhub.lessons.lesson3.task2;

import java.util.HashMap;
import java.util.Map;

public class Authorization {
    private Map<String, User> users = new HashMap<String, User>() {
        {
            put("User1", new User("User1", "user1"));
            put("User2", new User("User2", "user2"));
            put("User3", new User("User3", "user3"));
            put("User4", new User("User4", "user4"));
        }
    };

    public User auth(String login, String password) throws AuthException {
        if (login.equals("") || password.equals(""))
            throw new WrongCredentialsException();

        if (!users.containsKey(login))
            throw new UserNotFoundException();

        User res = users.get(login);

        if (!res.equalsPass(password))
            throw new WrongPasswordException();

        return res;
    }
}
