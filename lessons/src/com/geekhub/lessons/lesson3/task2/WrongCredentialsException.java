package com.geekhub.lessons.lesson3.task2;

public class WrongCredentialsException extends AuthException {
    public WrongCredentialsException () {
        super("Value password or login is empty");
    }

    public WrongCredentialsException(String message) {
        super("Value password or login is empty. Details: "+ message);
    }
}
