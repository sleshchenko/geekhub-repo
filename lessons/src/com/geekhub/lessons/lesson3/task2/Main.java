package com.geekhub.lessons.lesson3.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Input login...");
        String login = input.next();

        System.out.println("Input password...");
        String password = input.next();

        Authorization regLog = new Authorization();
        try {
            User user = regLog.auth(login, password);
            System.out.println("Hello " + user.getLogin() + ". Thank you for that logged in");
        } catch (AuthException e) {
            System.out.println(e.getMessage());
        }
    }
}
