package com.geekhub.lessons.lesson3.task2;

public class User {
    private String login;
    private String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean equalsPass(String password) {
        return this.password.equals(password);
    }
}
