package com.geekhub.lessons.lesson3.task2;

public class AuthException extends Exception {
    public AuthException () {
        super("Error Authorization");
    }

    public AuthException(String message) {
        super("Error Authorization. " + message);
    }
}
