package com.geekhub.lessons.lesson3.task2;

public class UserNotFoundException extends AuthException {
    public UserNotFoundException () {
        super("User does not exist");
    }

    public UserNotFoundException(String message) {
        super("User does not exist. Details: "+ message);
    }
}
