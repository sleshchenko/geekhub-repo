package com.geekhub.lessons.lesson3.task2;

public class WrongPasswordException extends AuthException {
    public WrongPasswordException () {
        super("Wrong password");
    }

    public WrongPasswordException(String message) {
        super("Wrong password. Details: " + message);
    }
}
