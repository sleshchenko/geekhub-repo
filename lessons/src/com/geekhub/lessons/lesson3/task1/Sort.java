package com.geekhub.lessons.lesson3.task1;

public class Sort {

    public static Comparable[] sort(Comparable[] elements) {
        elements = elements.clone();
        Comparable temp;   //holding variable

        boolean flag = true;   // set flag to true to begin first pass
        while (flag) {
            flag = false;    //set flag to false awaiting a possible swap
            for (int j = 0; j < elements.length - 1; j++) {
                if (elements[j].compareTo(elements[j + 1]) > 0)   // change to > for ascending sort
                {
                    temp = elements[j];                //swap elements
                    elements[j] = elements[j + 1];
                    elements[j + 1] = temp;
                    flag = true;              //shows a swap occurred
                }
            }
        }
        return elements;
    }
}
