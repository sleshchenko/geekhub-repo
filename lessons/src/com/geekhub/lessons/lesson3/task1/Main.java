package com.geekhub.lessons.lesson3.task1;

import java.util.Arrays;

import static com.geekhub.lessons.lesson3.task1.Sort.*;

public class Main {
    public static void test(Comparable[] elements) {
        System.out.println("Sorted array");
        System.out.println(Arrays.toString(sort(elements)));
        System.out.println("Input array");
        System.out.println(Arrays.toString(elements));
    }

    public static void main(String[] args) {
        System.out.println("------ >>> Integer <<< ------");
        Comparable[] arr = {12, 3, 4, 2, 35, 34, 23};
        test(arr);

        System.out.println("------ >>> Float <<< ------");
        arr = new Comparable[]{21.32, 2.1, 32.12, 1.1111, 43.32, 23.432, 4.554, 3.224};
        test(arr);

        System.out.println("------ >>> Character <<< ------");
        arr = new Comparable[]{'a', 'v', 'z', 'q', 'd', 'd', 'a', 'c', 't', 'e'};
        test(arr);

        System.out.println("------ >>> String <<< ------");
        arr = new Comparable[]{"Jaffa orange", "Apple", "Lime", "Avocado", "Citrus", "Cherry"};
        test(arr);
    }
}
