package com.geekhub.lessons.lesson3.task4;

import java.util.Scanner;

public class task4 {
    public static Integer parseInt(String str) throws NumberFormatException {
        if (str.charAt(0) < 49 || str.charAt(0) > 57)
            throw new NumberFormatException("Number can not begin with 0");
        for (int i = 0 ; i < str.length(); i++) {
            if (str.charAt(i) < 48 || str.charAt(i) > 57)
                throw new NumberFormatException("Contains characters that are not numbers");
        }

        Integer res = str.charAt(0) - 48;
        for (int i = 1; i < str.length(); i++) {
            res *= 10;
            res += (str.charAt(i) -48);
        }

        return res;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Input number...");
        String number = input.next();

        try {
            System.out.println(parseInt(number));
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

}
