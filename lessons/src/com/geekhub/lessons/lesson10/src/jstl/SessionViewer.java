package jstl;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Enumeration;

public class SessionViewer extends SimpleTagSupport {
    private HttpSession session;
    private String urlAdd;
    private String urlDelete;

    public void setUrlAdd(String urlAdd) {
        this.urlAdd = urlAdd;
    }

    public void setUrlDelete(String urlDelete) {
        this.urlDelete = urlDelete;
    }

    public void setSession(Object session) {
        this.session = (HttpSession) session;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>Name</td>");
        out.println("<td>Value</td>");
        out.println("<td></td>");
        out.println("</tr>");
        Enumeration<String> names = session.getAttributeNames();
        while (names.hasMoreElements()) {
            out.println("<tr>");
            String name = names.nextElement();
            Object value = session.getAttribute(name);
            out.println("<td>"+name+"</td>");
            out.println("<td>"+value+"</td>");
            out.println("<td><a href='/"+urlDelete+"?name="+name+"'>Delete</a></td>");
            out.println("</tr>");
        }

        out.println("<form method='GET' action='"+urlAdd+"'>");
        out.println("<tr>");
        out.println("<td><input name='name'/></td>");
        out.println("<td><input name='value'/></td>");
        out.println("<td><button type='submit'>Add</button></td>");
        out.println("</tr>");
        out.println("</form>");
        out.println("</table>");
    }
}
