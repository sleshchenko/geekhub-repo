package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addValue")
public class AddSessionValue extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String value = req.getParameter("value");
        if (!name.isEmpty() && !value.isEmpty()) {
            req.getSession().setAttribute(req.getParameter("name"), req.getParameter("value"));
        }

        resp.sendRedirect("/sessionManager");
    }


}
