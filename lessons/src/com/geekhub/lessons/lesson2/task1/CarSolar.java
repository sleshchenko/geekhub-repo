package com.geekhub.lessons.lesson2.task1;

public class CarSolar extends Vehicle{
    CarSolar() {
        source = new Battery(0,100);
    }
    CarSolar(double maxCharge, double startCharge) {
        source = new Battery(startCharge, maxCharge);
    }
    public String toString(){
        return " ------------- >> Information about CarSolar << ----------------\n" + super.toString();
    }
}
