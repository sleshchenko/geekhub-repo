package com.geekhub.lessons.lesson2.task1;

public class GasTank implements EnergySource{
    private double fuel;
    private double capacity;

    public GasTank(double fuel, double capacity) {
        this.fuel = fuel;
        this.capacity = capacity;
    }

    public double getCapacity() {
        return capacity;
    }

    public double getFuel() {
        return fuel;
    }

    public boolean addFuel(double count) {
        if (count <= 0)
            return false;

        if (fuel + count <= capacity) {
            fuel += count;
            return true;
        }

        return false;
    }

    public boolean useFuel(double count) {
        if (count < 0)
            return false;
        if (count > fuel)
            return false;
        fuel -= count;
        return true;
    }

    // adaptation of the fuel tank to the interface energy source

    @Override
    public double getEnergy() {
        return getFuel();
    }

    @Override
    public boolean addEnergy(double count) {
        return addFuel(count);
    }

    @Override
    public boolean useEnergy(double count) {
        return useFuel(count);
    }
}