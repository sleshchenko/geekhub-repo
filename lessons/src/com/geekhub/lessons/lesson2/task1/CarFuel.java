package com.geekhub.lessons.lesson2.task1;

public class CarFuel extends Vehicle {
    CarFuel() {
        source = new GasTank(0, 100);
    }
    CarFuel(double capacity, double startFuel) {
        source = new GasTank(startFuel, capacity);
    }

    public String toString(){
        return " ------------- >> Information about CarFuel << ----------------\n" + super.toString();
    }
}
