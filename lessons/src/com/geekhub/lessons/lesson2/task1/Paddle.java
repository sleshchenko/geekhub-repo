package com.geekhub.lessons.lesson2.task1;

public class Paddle {
    double degree; // if degree is < 0 - left, > 0 rigth, 0 - directly

    public double getDegree() {
        return degree;
    }

    public void turn(double degree) {
        this.degree += degree;
    }
}
