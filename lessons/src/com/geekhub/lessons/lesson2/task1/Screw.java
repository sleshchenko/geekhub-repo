package com.geekhub.lessons.lesson2.task1;

public class Screw {
    double rate;

    public double getRate() {
        return rate;
    }

    public void changeRateOn(double rate) {
        this.rate += rate;
    }
}
