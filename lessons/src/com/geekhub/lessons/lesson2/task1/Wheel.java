package com.geekhub.lessons.lesson2.task1;

public class Wheel {
    double rate;
    double degree; // if degree is < 0 - left, > 0 rigth, 0 - directly

    public double getDegree() {
        return degree;
    }

    public void changeDegreeOn(double degree) {
        this.degree += degree;
    }

    public double getRate() {
        return rate;
    }

    public void changeRateOn(double rate) {
        this.rate += rate;
    }
}
