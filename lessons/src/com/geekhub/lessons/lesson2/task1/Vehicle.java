package com.geekhub.lessons.lesson2.task1;

abstract public class Vehicle implements Drivable {
    protected EnergySource source;
    protected Engine engine;
    // This implementation allows for the wheels as the drive front car
    // and rear the drive or all wheel drive
    protected Wheel[] rotatingWheels; // wheels to turn (rotate)
    protected Wheel[] ratingWheels; // rotating wheel (speed)

    public double getSpeed() {
        return ratingWheels[0].getRate();
    }

    public double getRotate() {
        return rotatingWheels[0].getDegree();
    }

    public Vehicle() {
        engine = new Engine();

        rotatingWheels = new Wheel[2];
        for (int i = 0; i < rotatingWheels.length; i++) {
            rotatingWheels[i] = new Wheel();
        }

        ratingWheels = new Wheel[2];
        for (int i = 0; i < ratingWheels.length; i++) {
            ratingWheels[i] = new Wheel();
        }
    }

    public boolean tankUp(double countEnergy) {
        return source.addEnergy(countEnergy);
    }

    @Override
    public void turn(double degree) {
        for (Wheel rotatingWheel : rotatingWheels) {
            rotatingWheel.changeDegreeOn(degree);
        }
    }

    // 0 < coefficient <= 1 - shows how hard pressed the gas pedal
    @Override
    public boolean accelerate(double coefficient) {
        // determined to be engine speed in such a pressure on the gas pedal
        int turnoverUpTo = (int) Math.round(engine.getMaxTurnover() * coefficient);
        if (source.useEnergy(engine.getConsumptionOn1kTurnover() * turnoverUpTo / 1000)) {
            engine.accelerate(turnoverUpTo);
            for (Wheel wheel : ratingWheels) {
                wheel.changeRateOn(engine.getTurnover() / 50);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean brake(double coefficient) {
        // determined to be engine speed in such a pressure on the brake pedal
        int turnoverMinus = (int) Math.round(engine.getMaxTurnover() * coefficient);
        int turnoverDownTo = engine.getTurnover() - turnoverMinus;
        if (source.useEnergy(engine.getConsumptionOn1kTurnover() * turnoverDownTo / 1000)) {
            engine.brake(turnoverMinus);
            for (Wheel wheel : ratingWheels) {
                wheel.changeRateOn(-turnoverMinus / 50);
            }
            return true;
        }
        return false;
    }

    public String toString() {
        String res = "";
        res += "Engine: turnover = " + engine.getTurnover() + " maxTurnover = " + engine.getMaxTurnover() +
                " consumptionOn1kTurnover = " + engine.getConsumptionOn1kTurnover() + "\n";
        res += "SourceEnergy: Energy = " + source.getEnergy() + " capacity = " + source.getCapacity() + "\n";
        res += "Information about Wheel\n";
        double rotate = getRotate();
        String side;
        if (rotate > 0)
            side = "right";
        else if (rotate < 0)
            side = "left";
        else side = "directly";
        res += "Wheel Rotating: degree = " + (rotate == 0 ? side : side + " " + Math.abs(rotate)) + "\n";
        res += "Wheel Rating: rate = " + getSpeed() + "\n";
        res += " -------------- >>> End <<< -------------------";
        return res;
    }
}
