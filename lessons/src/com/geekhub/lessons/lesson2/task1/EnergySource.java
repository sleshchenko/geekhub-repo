package com.geekhub.lessons.lesson2.task1;

public interface EnergySource {
    public double getCapacity();
    double getEnergy();
    public boolean addEnergy(double count);
    public boolean useEnergy(double count);
}
