package com.geekhub.lessons.lesson2.task1;

public interface Drivable {
    abstract void turn(double degree);

    abstract boolean accelerate(double coefficient);

    abstract boolean brake(double coefficient);
}
