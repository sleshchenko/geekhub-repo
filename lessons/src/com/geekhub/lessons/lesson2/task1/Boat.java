package com.geekhub.lessons.lesson2.task1;

public class Boat implements Drivable {
    private Engine engine;
    private GasTank gasTank;
    private Screw screw; // used for speed control
    private Paddle[] paddle; // used to rotate

    Boat() {
        engine = new Engine();
        gasTank = new GasTank(0, 100);
        screw = new Screw();
        paddle = new Paddle[1];
        paddle[0]  = new Paddle();
    }

    Boat(double capacityGasTank, double startFuel) {
        engine = new Engine();
        gasTank = new GasTank(startFuel, capacityGasTank);
        screw = new Screw();
        paddle = new Paddle[1];
        paddle[0]  = new Paddle();
    }

    public boolean tankUp(double countFuel) {
        return gasTank.addFuel(countFuel);
    }

    @Override
    public void turn(double degree) {
        for (Paddle paddle: this.paddle)
            paddle.turn(degree);
    }

    @Override
    public boolean accelerate(double coefficient) {
        int turnoverUpTo = (int) Math.round(engine.getMaxTurnover() * coefficient);
        if (gasTank.useFuel(engine.getConsumptionOn1kTurnover() * turnoverUpTo / 1000)) {
            engine.accelerate(turnoverUpTo);
            screw.changeRateOn(engine.getTurnover() / 50);
            return true;
        }
        return false;
    }

    @Override
    public boolean brake(double coefficient) {
        int turnoverMinus = (int) Math.round(engine.getMaxTurnover() * coefficient);
        int turnoverDownTo = engine.getTurnover() - turnoverMinus;
        if (gasTank.useFuel(engine.getConsumptionOn1kTurnover() * turnoverDownTo / 1000)) {
            engine.brake(turnoverMinus);
            screw.changeRateOn(-turnoverMinus / 50);
            return true;
        }
        return false;
    }

    public double getSpeed() {
        return  screw.getRate();
    }

    public double getRotate() {
        return  paddle[0].getDegree();
    }

    public String toString() {
        String res = "------------- >> Information about Boat << ----------------\n";
        res += "Engine: turnover = " + engine.getTurnover() + " maxTurnover = " + engine.getMaxTurnover() +
                " consumptionOn1kTurnover = " + engine.getConsumptionOn1kTurnover() + "\n";
        res += "SourceEnergy: Energy = " + gasTank.getFuel() + " capacity = " + gasTank.getCapacity() + "\n";
        res += "Information about Wheel\n";
        double rotate = getRotate();
        String side;
        if (rotate > 0)
            side = "right";
        else if (rotate < 0)
            side = "left";
        else side = "directly";
        res += "Wheel Rotating: degree = " + (rotate == 0 ? side : side + " " + Math.abs(rotate)) + "\n";
        res += "Wheel Rating: rate = " + getSpeed() + "\n";
        res += " -------------- >>> End <<< -------------------";
        return res;
    }
}
