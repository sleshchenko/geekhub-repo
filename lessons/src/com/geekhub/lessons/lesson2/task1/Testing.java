package com.geekhub.lessons.lesson2.task1;

public class Testing {

    public static void main(String[] args) {
        Drivable drive = new Boat(100,100);
        drive.accelerate(1);
        drive.turn(-1);
        drive.brake(0.3);
        System.out.println("Speed +1, turn -1, brake 0.3");
        System.out.println(drive);

        drive = new CarFuel(100,100);
        drive.accelerate(0.5);
        drive.turn(+1);
        drive.brake(0.1);
        System.out.println("\nSpeed +0.5, turn +1, brake 0.1");
        System.out.println(drive);

        drive = new CarSolar(100,100);
        drive.accelerate(0.7);
        drive.turn(+2);
        drive.brake(0.5);
        System.out.println("\nSpeed +0.7, turn +2, brake 0.5");
        System.out.println(drive);
    }

}
