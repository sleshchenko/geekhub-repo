package com.geekhub.lessons.lesson2.task1;

public class Engine {
    private int turnover;
    private int maxTurnover;
    private double consumptionOn1kTurnover; //use of fuel at 1000 rpm

    public Engine(int maxTurnover) {
        this.maxTurnover = maxTurnover;
        this.consumptionOn1kTurnover = 5;
    }

    public Engine() {
        this.maxTurnover = 3500;
        this.consumptionOn1kTurnover = 5;
    }

    public double getConsumptionOn1kTurnover() {
        return consumptionOn1kTurnover;
    }

    public int getMaxTurnover() {
        return maxTurnover;
    }

    public int getTurnover() {
        return turnover;
    }

    public void accelerate(int turnover) {
        if (turnover <= 0)
            return;

        if (this.turnover + turnover > maxTurnover)
            this.turnover = maxTurnover;
        else
            this.turnover += turnover;
    }

    public void brake(int turnover) {
        if (turnover <= 0)
            return;
        this.turnover -= turnover;
        if (this.turnover < 0)
            this.turnover = 0;
    }
}
