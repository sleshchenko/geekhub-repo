package com.geekhub.lessons.lesson2.task1;

public class Battery implements EnergySource{
    private double charge;
    private double maxCharge;

    Battery(double charge, double maxCharge) {
        this.charge = charge;
        this.maxCharge = maxCharge;
    }

    public double getMaxCharge() {
        return maxCharge;
    }

    public double getCharge() {
        return charge;
    }

    public boolean addCharge(double count) {
        if (count <= 0)
            return false;

        if (charge + count <= maxCharge) {
            charge += count;
            return true;
        }

        return false;
    }

    public boolean useCharge(double count) {
        if (count < 0)
            return false;
        if (count > charge)
            return false;
        charge -= count;
        return true;
    }

    //adaptation of the battery to the interface energy source

    @Override
    public double getCapacity() {
        return getMaxCharge();
    }

    @Override
    public double getEnergy() {
        return getCharge();
    }

    @Override
    public boolean addEnergy(double count) {
        return addCharge(count);
    }

    @Override
    public boolean useEnergy(double count) {
        return useCharge(count);
    }
}
