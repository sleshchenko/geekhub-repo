package com.geekhub.lessons.lesson6.json.adapters;

import com.geekhub.lessons.lesson6.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        JSONArray m = new JSONArray();
        for (Object item : c) {
            m.put(JsonSerializer.serialize(item));
        }
        return m;
    }
}
