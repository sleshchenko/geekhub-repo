package com.geekhub.lessons.lesson1.task1;

import java.util.*;

public class task1 {

    static long fact(long n) {
        if (n == 1)
            return 1;
        else return n * fact(n-1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputNumber;

        System.out.println("Please input integer value:");
        while (!scanner.hasNextInt()) {
            System.out.println("Error: is not valid integer! Try again!");
            scanner.next();
        }

        inputNumber = scanner.nextInt();
        System.out.println("Factorial of " + inputNumber + " is " + fact(inputNumber));
    }

}