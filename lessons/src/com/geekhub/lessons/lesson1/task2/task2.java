package com.geekhub.lessons.Lesson1.task2;

import java.util.*;

public class task2 {

    static void PrintFirstNFibonacci(int N) {
        System.out.print("Fibonacci sequence: ");
        int a = 0;
        int b = 1;
        for (int i = 0; i < N-1; i++) {
            System.out.print(b + ", ");
            int c = a + b;
            a = b;
            b = c;
        }
        System.out.print(b);
        System.out.println(";");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputNumber;

        System.out.println("Please input integer value:");
        while (!scanner.hasNextInt()) {
            System.out.println("Error: is not valid integer! Try again!");
            scanner.next();
        }

        inputNumber = scanner.nextInt();

        PrintFirstNFibonacci(inputNumber);
    }
}
