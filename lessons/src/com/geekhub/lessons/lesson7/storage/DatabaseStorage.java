package com.geekhub.lessons.lesson7.storage;

import com.geekhub.lessons.lesson7.objects.Entity;
import com.geekhub.lessons.lesson7.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link com.geekhub.lessons.lesson7.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link com.geekhub.lessons.lesson7.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            int result = statement.executeUpdate(sql);
            return result != 0;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String sql = null;
        if (entity.isNew()) {
            sql = "INSERT INTO " + entity.getClass().getSimpleName() + " (";

            Set<Map.Entry<String, Object>> keyValue = data.entrySet();

            int index = 0;
            for (Map.Entry<String, Object> entry : keyValue) {
                if (index++ < data.size() - 1) {
                    sql += entry.getKey() + ", ";
                } else {
                    sql += entry.getKey() + ")";
                }
            }

            sql += " values(";
            for (int i = 0; i < data.size() - 1; i++) {
                sql += "?, ";
            }
            sql += "?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                index = 1;
                for (Map.Entry<String, Object> entry : keyValue) {
                    preparedStatement.setObject(index++, entry.getValue());
                }
                preparedStatement.executeUpdate();
                ResultSet rs = preparedStatement.getGeneratedKeys();
                rs.next();
                entity.setId(rs.getInt(1));
            }
        } else {
            sql = "UPDATE " + entity.getClass().getSimpleName() + " SET ";

            Set<Map.Entry<String, Object>> keyValue = data.entrySet();
            int index = 0;
            for (Map.Entry<String, Object> entry : keyValue) {
                if (index++ < data.size()-1) {
                    sql += " " + entry.getKey() + "=?, ";
                } else {
                    sql += " " + entry.getKey() + "=?";
                }
            }

            sql += " WHERE id = " + entity.getId();

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                index = 1;
                for (Map.Entry<String, Object> entry : keyValue) {
                    preparedStatement.setObject(index++, entry.getValue());
                }

                preparedStatement.executeUpdate();
            }
        }
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> result = new HashMap<>();

        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                field.setAccessible(true);
                result.put(field.getName(), field.get(entity));
                field.setAccessible(false);
            }
        }

        return result;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        Field[] fields = clazz.getDeclaredFields();
        List<T> result = new ArrayList<>();

        while (resultset.next()) {
            T instance = clazz.newInstance();

            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    field.set(instance, resultset.getObject(field.getName()));
                    field.setAccessible(false);
                }
            }

            instance.setId(resultset.getInt("id"));

            result.add(instance);
        }

        return result;
    }
}
