package com.geekhub.lessons.lesson8;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream res = new ByteArrayOutputStream();

        try (InputStream inputStream = url.openStream()) {
            byte[] buffer = new byte[1024 * 10];
            int len;
            while((len = inputStream.read(buffer)) > 0) {
                res.write(buffer, 0, len);
            }
        }

        return res.toByteArray();
    }
}
