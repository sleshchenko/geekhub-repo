package com.geekhub.servlets;

import com.geekhub.entities.Movie;
import com.geekhub.services.MoviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.Calendar;

@Controller

public class MoviesController {
    private MoviesService moviesService;

    @Autowired
    public MoviesController(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public String showMovies(ModelMap model) throws SQLException {
        model.addAttribute("movies", moviesService.getMovies());
        model.addAttribute("countActors", moviesService.getCountActors());
        return "movies";
    }

    @RequestMapping(value = "/movie", method = RequestMethod.GET)
    public String showMovie(@RequestParam(required = false) Integer id, ModelMap model) throws SQLException {
        if (id != null) {
            model.addAttribute("movie", moviesService.getById(id));
        }
        return "movie";
    }

    @RequestMapping(value = "/deletemovie", method = RequestMethod.GET)
    public String deleteMovie(@RequestParam(required = true) Integer id) throws SQLException {
        moviesService.delete(id);
        return "redirect:/movies";
    }

    @RequestMapping(value = "/updatemovie", method = RequestMethod.POST)
    public String editMovie(@RequestParam(required = false) Integer id, @RequestParam String name,
                               @RequestParam Integer year)
            throws SQLException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        Movie movie = new Movie()
                .withName(name)
                .withYear(calendar.getTime());
        if (id == null) {
            moviesService.create(movie);
        } else {
            moviesService.update(movie.withId(id));
        }
        return "redirect:/movies";
    }
}