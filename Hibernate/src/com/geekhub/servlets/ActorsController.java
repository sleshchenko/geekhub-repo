package com.geekhub.servlets;

import com.geekhub.entities.Actor;
import com.geekhub.services.ActorsService;
import com.geekhub.services.MoviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Controller

public class ActorsController {
    private ActorsService actorService;
    private MoviesService moviesService;

    @Autowired
    public ActorsController(ActorsService actorService, MoviesService moviesService) {
        this.actorService = actorService;
        this.moviesService = moviesService;
    }

    @RequestMapping(value = "/actors", method = RequestMethod.GET)
    public String showActors(ModelMap model) throws SQLException {
        model.addAttribute("actors", actorService.getActors());
        model.addAttribute("countFilms", actorService.getCountFilms());
        return "actors";
    }

    @RequestMapping(value = "/actor", method = RequestMethod.GET)
    public String showActor(@RequestParam(required = false) Integer id, ModelMap model) throws SQLException {
        if (id != null) {
            model.addAttribute("actor", actorService.getById(id));
            model.addAttribute("selectedMovies", actorService.getMovies(id));
        }
        model.addAttribute("movies", moviesService.getMovies());
        return "actor";
    }

    @RequestMapping(value = "/deleteactor", method = RequestMethod.GET)
    public String deleteMovie(@RequestParam(required = true) Integer id) throws SQLException {
        actorService.delete(id);
        return "redirect:/actors";
    }

    @RequestMapping(value = "/updateactor", method = RequestMethod.POST)
    public String editActor(@RequestParam(required = false) Integer id, @RequestParam String name,
                            @RequestParam String surname, @RequestParam String birth,
                            @RequestParam(required = false) Integer[] movies)
            throws SQLException, ParseException {
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
        Actor actor = new Actor()
                .withName(name)
                .withSurname(surname)
                .withBirth(df.parse(birth));
        if (id == null) {
            actorService.create(actor);
            actorService.updateLinks(actor.getId(), movies);
        } else {
            actorService.update(actor.withId(id));
            actorService.updateLinks(actor.getId(), movies);
        }
        return "redirect:/actors";
    }
}