package com.geekhub.entities;

import java.util.Date;

public class Actor {
    private int id;
    private String name;
    private String surname;
    private Date birth;
    private int countFilm;

    public Actor() {
    }

    public Actor(int id, String name, String surname, Date birth, int countFilm) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birth = birth;
        this.countFilm = countFilm;
    }

    public int getId() {
        return id;
    }

    public int getCountFilm() {
        return countFilm;
    }

    public void setCountFilm(int countFilm) {
        this.countFilm = countFilm;
    }

    public Actor withCountFilm(int countFilm) {
        this.countFilm = countFilm;
        return this;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Actor withId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getBirth() {
        return birth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Actor withName(String name) {
        this.name = name;
        return this;
    }

    public Actor withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public Actor withBirth(Date birth) {
        this.birth = birth;
        return this;
    }
}
