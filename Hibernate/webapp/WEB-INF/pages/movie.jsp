<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>
<div class="wrapper">
    <form method="POST" action="/updatemovie">
        <div class="body">
            <center>
                <table class="table" style="font-size: 20px;">
                    <tr class="tableHeader">
                        <td colspan="2" style="text-align: center">Add/Edit Actor</td>
                    </tr>
                    <input name="id" value="<c:if test="${not empty movie.id}">${movie.id}</c:if>" style="display: none;"/>
                    <tr>
                        <td>Name</td>
                        <td><input class="input" name="name" value="<c:if test="${not empty movie.id}">${movie.name}</c:if>"/></td>
                    </tr>
                    <tr>
                        <td>Year</td>
                        <td><input class="input" name="year" value="<fmt:formatDate type="both" pattern="yyyy" value="${movie.year}" />"/></td>
                    </tr>
                    <tr style="text-align: center; height: 60px;">
                        <td>
                            <button type="submit" class="btn">Save</button>
                        </td>
                        <td>
                            <button class="btn" onclick="document.location = '/movies';">Cancel</button>
                        </td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</div>
</body>
</html>